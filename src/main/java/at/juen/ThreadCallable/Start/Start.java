package at.juen.ThreadCallable.Start;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import at.juen.ThreadCallable.Calc.CalcF;
import at.juen.ThreadCallable.Output.CMD;
import at.juen.ThreadCallable.Output.CSV;
import at.juen.ThreadCallable.Output.Output;

public class Start {

	private List<Double> f;
	private List<Integer> n;
	private List<Future<Object>> resList;
	private List<Output> outputList;
	
	public Start(){
		f = new ArrayList<Double>();
		n = new ArrayList<Integer>();
		resList = new ArrayList<Future<Object>>();
		outputList = new ArrayList<Output>();
		outputList.add(new CSV());
		outputList.add(new CMD());
		
		f.add(0.01);
		f.add(0.05);
		f.add(0.1);
		f.add(0.2);
		f.add(0.3);
		f.add(0.4);
		f.add(0.5);
		f.add(0.6);
		f.add(0.7);
		f.add(0.8);
		f.add(0.9);
		f.add(0.99);
		
		for(int i = 1; i < 17; i++){
			n.add(i);
		}
		
		ExecutorService pool = Executors.newFixedThreadPool(12);
		for(int a = 0; a < f.size(); a ++){
			resList.add(pool.submit(new CalcF(n,f.get(a))));
		}
		
		for(int b = 0; b < outputList.size(); b++){
			outputList.get(b).setResult(resList);
			outputList.get(b).writeNFx();
			outputList.get(b).cleanUp();
		}
	}
	
	public static void main(String[] args) {
		new Start();
	}	
}
