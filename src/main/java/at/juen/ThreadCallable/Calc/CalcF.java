package at.juen.ThreadCallable.Calc;

import java.util.List;
import java.util.concurrent.Callable;

import at.juen.ThreadCallable.Data.Result;

public class CalcF implements Callable<Object>{
	
	private List<Integer> n;
	private double f;

	public CalcF(List<Integer> n, double f){
		this.n = n;
		this.f = f;
	}	

	public Object call() throws Exception {
		return calc();
	}
	
	public Result calc(){
		Result ret = new Result();
		ret.setN(n);
		ret.setF(f);
		
		for(int i = 0; i < n.size(); i++){
			double s = 1/(f+((1-f)/n.get(i)));
			ret.getS().add(s);
		}		
		return ret;
	}

}
