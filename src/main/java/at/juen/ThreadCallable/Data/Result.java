package at.juen.ThreadCallable.Data;

import java.util.ArrayList;
import java.util.List;

public class Result {
	
	public Result(){
		s = new ArrayList<Double>();
	}
	
	private double f;
	private List<Double> s;
	private List<Integer> n;
	
	public List<Double> getS() {
		return s;
	}
	
	public void setS(List<Double> s) {
		this.s = s;
	}
	
	public double getF() {
		return f;
	}
	
	public void setF(double f) {
		this.f = f;
	}
	
	public List<Integer> getN() {
		return n;
	}
	
	public void setN(List<Integer> n) {
		this.n = n;
	}
}
