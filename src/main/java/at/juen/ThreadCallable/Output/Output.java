package at.juen.ThreadCallable.Output;

import java.util.List;
import java.util.concurrent.Future;

public interface Output {

	void setResult(List<Future<Object>> result);
	
	void writeNFx();
	
	void writeNFS();
	
	void cleanUp();
}
