package at.juen.ThreadCallable.Output;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import at.juen.ThreadCallable.Data.Result;

public class CSV implements Output{
	private List<Result> results;
	private StringBuilder sb;
	private String output;
	private FileWriter fw;
	private BufferedWriter bw;
	
	public CSV(){
		sb = new StringBuilder();
		try{
			fw = new FileWriter("D:\\ThreadCallable.csv");
			bw = new BufferedWriter(fw);
		}
		catch(IOException e){
			System.out.println(e.getMessage());
		}
	}
	
	public void setResult(List<Future<Object>> result){
		this.results =  new ArrayList<Result>();
		for(int i = 0; i < result.size(); i++){
			try {
				this.results.add((Result) result.get(i).get());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void writeNFS(){
		try{
			if(results != null){	
				bw.write("n;F;S");
				bw.newLine();
				DecimalFormat f = new DecimalFormat("#0.00");
				for(int a = 0; a < results.size(); a++){
					for(int i = 0; i < results.get(a).getS().size(); i++){
				
						sb.setLength(0);
						sb.append(f.format(results.get(a).getN().get(i)));
						sb.append(";");
						sb.append(f.format(results.get(a).getF()));
						sb.append(";");
						sb.append(f.format(results.get(a).getS().get(i)));
						output = sb.toString();
				
						bw.write(output);
						bw.newLine();
					}
				}
				bw.close();
			}
		}
		catch(IOException e){
			System.out.println(e.getMessage());
		}
		finally{
			try{
				fw.close();
				bw.close();
			}
			catch(IOException e){
				System.out.println(e.getMessage());
			}
		}
	}
	
	public void writeNFx(){
		try{
			if(results != null){
				String head;
				sb.append("n");
				for(int i = 0; i < results.size(); i++){
					sb.append(";");
					sb.append("F = ");
					sb.append(results.get(i).getF());
				}
				head = sb.toString();
				bw.write(head);
				bw.newLine();
				sb.setLength(0);
				DecimalFormat f = new DecimalFormat("#0.00");
				
				for(int i = 0; i < results.get(0).getN().size(); i++){
					sb.setLength(0);
					sb.append(i+1);
					for(int a = 0; a < results.size(); a++){
						sb.append(";");
						sb.append(f.format(results.get(a).getS().get(i)));
						
					}
					bw.write(sb.toString());
					bw.newLine();
				}
				bw.close();
			}
		}
		catch(IOException e){
			System.out.println(e.getMessage());
		}
		finally{
			try{
				fw.close();
				bw.close();
			}
			catch(IOException e){
				System.out.println(e.getMessage());
			}
		}
	}
	
	public String getOutput(){
		return output;
	}
	
	public void cleanUp(){
		try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
